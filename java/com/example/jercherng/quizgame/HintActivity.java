package com.example.jercherng.quizgame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by JerCherng on 2/15/2016.
 */
public class HintActivity extends AppCompatActivity {

    // for StateChange use only:
    private int lastQuestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hint);

        Intent activityThatCalled = getIntent();

        Question question = new Question(activityThatCalled.getExtras().getInt("questionIndex"));

        TextView questionBox = (TextView) findViewById(R.id.question_box);
        String questionIs = "Question is:\n" + question.getQuestion();
        questionBox.setText(questionIs);

        TextView hintBox = (TextView) findViewById(R.id.hint_box);
        hintBox.setText(question.getHints());
    }


    public void onReturnButtonClick(View view) {
        super.finish();
    }
}
