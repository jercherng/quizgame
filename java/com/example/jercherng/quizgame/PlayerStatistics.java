package com.example.jercherng.quizgame;

/**
 * Created by JerCherng on 2/10/2016.
 */



public class PlayerStatistics {

    private int attempted;
    private int correct;
    private int incorrect;

    public PlayerStatistics() {
        this.attempted = 0;
        this.correct = 0;
        this.incorrect = 0;
    }

    public PlayerStatistics(int attempted, int correct, int incorrect) {
        this.attempted = attempted;
        this.correct = correct;
        this.incorrect = incorrect;
    }

    public int getAttempted() {
        return attempted;
    }
    public void setAttempted() {
        this.attempted++;
    }

    public int getCorrect() {
        return correct;
    }
    public void setCorrect() {
        this.correct++;
        this.setAttempted();
    }

    public int getIncorrect() {
        return incorrect;
    }
    public void setIncorrect() {
        this.incorrect++;
        this.setAttempted();
    }
}
