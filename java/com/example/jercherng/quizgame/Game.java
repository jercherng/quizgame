package com.example.jercherng.quizgame;

import java.util.Random;

/**
 * Created by JerCherng on 2/10/2016.
 */
public class Game {

    private Question question;
    private PlayerStatistics playerStatistics;
    private boolean playerChoice;

    public Game() {
        this.question = new Question();
        this.playerStatistics = new PlayerStatistics();
        this.playerChoice = false;
    }

    public Game(int questionNumber, int lastAttempted, int lastCorrect, int lastIncorrect,
                boolean lastPlayerChoice) {

        Question copyQuestion = new Question(questionNumber);
        this.setQuestion(copyQuestion);

        PlayerStatistics copyPlayerStatistics =
                new PlayerStatistics(lastAttempted, lastCorrect, lastIncorrect);
        this.setPlayerStatistics(copyPlayerStatistics);

        this.playerChoice = lastPlayerChoice;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
    public String getQuestion() {
        return this.question.getQuestion();
    }
    public int getQuestionIndex() {
        return this.question.getCount();
    }

    public void setPlayerChoice(boolean choice) {
        this.playerChoice = choice;
    }
    public boolean getPlayerChoice() {
        return this.playerChoice;
    }

    public void setPlayerStatistics(PlayerStatistics playerStatistics) {
        this.playerStatistics = playerStatistics;
    }
    public PlayerStatistics getPlayerStatistics() {
        return playerStatistics;
    }

    public int getPlayerAttempted() {
        return this.playerStatistics.getAttempted();
    }
    public int getPlayerCorrect() {
        return this.playerStatistics.getCorrect();
    }
    public int getPlayerIncorrect() {
        return this.playerStatistics.getIncorrect();
    }

    // Interaction Layout features are here
    // this is the only function where its not completely encapsulated.
    public boolean answer() {
        // gives the function the answer
        if(this.question.getRightAnswer(playerChoice)) {
            playerStatistics.setCorrect();
            return true;
        } else {
            playerStatistics.setIncorrect();
            return false;
        }
    }

    public void setNextQuestion() {
        Random random = new Random();
        this.question.setCount(random.nextInt(6));
    }

    public String getHintForQuestion() {
        return this.question.getHints();
    }
}
