package com.example.jercherng.quizgame;

/**
 * Created by JerCherng on 2/10/2016.
 */

public class Question {
    private String questions[] = {"1 == 2?", "2 >= 1", "Is tomato a fruit?",
            "Are Avocados fruits?", "BMW owns Mini, True or False?", "6 < 5"};

    private String hints[] = {"Is 1 equal to 2?",
            "Is 2 greater than or equal to 1?",
            "Technically speaking Tomatoes are fruits.",
            "Avocados fruits are fruits too.",
            "BMW bought Rover Group which owns Mini in 1994.",
            "6 is definitely not less than 5."};

    private boolean answers[] = {false, true, true, true, true, false};
    private int count;

    // constructor
    public Question() {
        // this sets the question.
        this.setCount(0);
    };

    public Question(int last_question) {
        // this sets the question.
        this.setCount(last_question);
    };

    // getters and setters
    public void setCount(int index) {
        this.count = index;
    }
    public int getCount() {
        return this.count;
    }

    public String getQuestion() {
        return this.questions[this.count];
    }
    public boolean getAnswer() {
        return this.answers[this.count];
    }

    public String getHints() {
        return hints[count];
    }

    public boolean getRightAnswer(boolean userChoice) {
        if(userChoice == answers[count])
            return true;
        return false;
    }
}
