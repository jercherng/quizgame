package com.example.jercherng.quizgame;

import android.content.Intent;

import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.os.CountDownTimer;

import android.view.View;

import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Game game;
    private boolean correct;

    private String correctAnswerMessage;
    private String incorrectAnswerMessage;

    // for StateChange use only:
    private int lastQuestion;
    private int lastAttempted;
    private int lastCorrect;
    private int lastIncorrect;
    private boolean lastPlayerChoice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeMessages();

        if(savedInstanceState != null) {
            lastQuestion = savedInstanceState.getInt("lastQuestion");
            lastAttempted = savedInstanceState.getInt("lastAttempted");
            lastCorrect = savedInstanceState.getInt("lastCorrect");
            lastIncorrect = savedInstanceState.getInt("lastIncorrect");
            lastPlayerChoice = savedInstanceState.getBoolean("lastPlayerChoice");

            // call copy constructor of game
            this.game =
                    new Game(lastQuestion, lastAttempted, lastCorrect, lastIncorrect,
                            lastPlayerChoice);
            this.correct = false;
        } else {
            this.game = new Game();
            this.correct = false;
        }
        TextView questionBox = (TextView) findViewById(R.id.question_box);
        questionBox.setText(this.game.getQuestion());
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.true_button:
                if (checked) {
                    // Pirates are the best
                    game.setPlayerChoice(true);
                }
                break;
            case R.id.false_button:
                if (checked) {
                    // Ninjas rule
                    game.setPlayerChoice(false);
                }
                break;
        }
    }

    // have to implement onclick for the following:
    // 1. answer
    // 2. hint
    // 3. next question
    public void onAnswerButtonClick(View view) {

        RadioButton trueButton = (RadioButton) findViewById(R.id.true_button);
        RadioButton falseButton = (RadioButton) findViewById(R.id.false_button);

        if ((trueButton.isChecked() || falseButton.isChecked())) {
            correct = this.game.answer();
            // here we have to pull the new info from the database
            this.updateStatisticsView();
            // use a toast to show user the results
            if (correct) {
                this.showToast(correctAnswerMessage);
            } else
                this.showToast(incorrectAnswerMessage);
        } else {
            this.showToast("Select an answer!");
        }
    }

    public void onHintButtonClick(View view) {
        Intent getHintActivityIntent = new Intent(this, HintActivity.class);
        getHintActivityIntent.putExtra("questionIndex", this.game.getQuestionIndex());
        startActivity(getHintActivityIntent);
    }

    public void onNextButtonClick(View view) {
        this.game.setNextQuestion();

        TextView questionBox = (TextView) findViewById(R.id.question_box);
        questionBox.setText(this.game.getQuestion());

        RadioGroup answerRadio = (RadioGroup) findViewById(R.id.answer_layout);
        answerRadio.clearCheck();
    }

    public void showToast(String message) {
        final Toast mToastToShow;
        // Set the toast and duration
        int toastDurationInMilliSeconds = 888;
        mToastToShow = Toast.makeText(this, message, Toast.LENGTH_LONG);

        // Set the countdown to display the toast
        CountDownTimer toastCountDown;
        toastCountDown = new CountDownTimer(toastDurationInMilliSeconds, 1000 /*Tick duration*/) {
            public void onTick(long millisUntilFinished) {
                mToastToShow.show();
            }
            public void onFinish() {
                mToastToShow.cancel();
            }
        };

        // Show the toast and starts the countdown
        mToastToShow.show();
        toastCountDown.start();
    }

    public void updateStatisticsView() {
        TextView attempted_view = (TextView) findViewById(R.id.attempted_value);
        attempted_view.setText(Integer.toString(this.game.getPlayerAttempted()));

        TextView correct_view = (TextView) findViewById(R.id.correct_value);
        correct_view.setText(Integer.toString(this.game.getPlayerCorrect()));

        TextView incorrect_view = (TextView) findViewById(R.id.incorrect_value);
        incorrect_view.setText(Integer.toString(this.game.getPlayerIncorrect()));
    }

    private void initializeMessages() {
        correctAnswerMessage = (String) getResources().getText(R.string.correct_answer);
        incorrectAnswerMessage = (String) getResources().getText(R.string.incorrect_answer);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // what do I need to save?
        // player stats: att, corr, incorr
        // current question: count
        savedInstanceState.putInt("lastQuestion", game.getQuestionIndex());
        savedInstanceState.putInt("lastAttempted", game.getPlayerAttempted());
        savedInstanceState.putInt("lastCorrect", game.getPlayerCorrect());
        savedInstanceState.putInt("lastIncorrect", game.getPlayerIncorrect());
        savedInstanceState.putBoolean("lastPlayerChoice", game.getPlayerChoice());
    }
}